
function cacheFunction(callback) {
    let object = {}
    function gettingValues(value){
        if (value in object){
            console.log(object[value])
        }
        else{
            object[value] = callback(value)
            console.log(object[value])
        }
    }
    return gettingValues;
}

module.exports = cacheFunction;