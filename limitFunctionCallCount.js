function limitFunctionCallCount(callback, number) {
    let counter = 0;
    function FunctionCall(...values){
        counter += 1
        if(counter > number){
            return null;
        }
        else {
             return callback(...values);
        }
    }
    return FunctionCall
}

module.exports = limitFunctionCallCount;