let cacheFunction = require('../cacheFunction')

function double(value){
    if (typeof(value) === "number"){
        return value*2;
    }
    else{
        return value;
    }
}
let output = cacheFunction(double)
output(5)
output("hello")
output([1,2])
output(2)
output(7)
output(5)