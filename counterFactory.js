
function counterFactory() {
    let startValue = 0;
    return {increment : function(){
                startValue += 1
                return startValue
            },
            decrement : function(){
                startValue -= 1;
                return startValue;
            }
    };
}

module.exports = counterFactory;